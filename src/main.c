#include <zephyr/kernel.h>
#include <zephyr/sys/printk.h> 
#include <stdio.h>   
#include <string.h>  

int main(void) {
	printk("Hello world\n");
	while(true){
		k_msleep(1000);
		printk("Hello again...\n");
	}
  return 0; 
}
